const { CommandoClient } = require('discord.js-commando');
const config = require("./config.json")
var fs = require('fs');
global.servers = {};
const client = new CommandoClient({
    commandPrefix: '!',
    unknownCommandResponse: true
});

client.registry
    .registerDefaultTypes()
    .registerGroup('simple', 'Our First Command Group')
    .registerGroup('music', 'Music Stuff')
    .registerDefaultGroups()
    .registerDefaultCommands()
    .registerCommandsIn(__dirname + '/commands');
fs.readdir("./servers", function(err, filenames) {
    if (err) {
        console.log(err);
        return;
    }
    filenames.forEach(function(filename) {
        var obj = JSON.parse(fs.readFileSync("./servers/" + filename, 'utf8'));
        servers[filename] = obj;
    });
});
client.on('ready', () => {
    console.log('Logged in!');
    client.user.setActivity('game');
});

client.login(config.TOKEN);