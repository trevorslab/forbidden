const { Command } = require('discord.js-commando');

module.exports = class JoinVoiceCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'list',
            aliases: ['list-songs'],
            group: 'music',
            memberName: 'list',
            description: 'lists the playlist',
            examples: ['list']
        });
    }

    run(msg, { link }) {
        var server = servers[msg.guild.id];
        var output = "";
        server.queue.forEach(function(link) {
            output = output + link + "\n";
        });
        msg.reply(output);
    }
};