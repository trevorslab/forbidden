const { Command } = require('discord.js-commando');
module.exports = class JoinVoiceCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'join',
            group: 'music',
            memberName: 'join',
            description: 'Joins the voice channel you are in',
            examples: ['join']
        });
    }

    run(msg) {
        var server = servers[msg.guild.id];
        if(msg.member.voiceChannel){
            if(!msg.guild.voiceConnection){
                server.queue = [];
                server.playing = false;
                msg.member.voiceChannel.join().then(msg.reply("Joined Successfuly"));
            }else msg.reply("I am already in a voice channel on this server");
        }else msg.reply("You are not in a voice channel on this server");
    }
};