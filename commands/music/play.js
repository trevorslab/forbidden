const { Command } = require('discord.js-commando');
const YTDL = require('ytdl-core')
function Play(msg){
    var server = servers[msg.guild.id];
    connection = msg.guild.voiceConnection;
    server.dispatcher = connection.playStream(YTDL(server.queue[0], {filter: "audioonly"}));
    server.playing = true;
    server.dispatcher.on("end",function(){
        server.queue.shift();
        console.log(server.queue[0]);
        if(server.queue[0] == undefined){
            console.log("ended");
            server.playing = false;
            msg.reply("End of queue");
        }else{
            Play(msg);
            console.log("nextsoin");
        }
    });
}
module.exports = class PlayCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'play',
            aliases: ['playsongs'],
            group: 'music',
            memberName: 'play',
            description: 'Plays the queue',
            examples: ['add [Video URL]']
        });
    }

    run(msg) {
        var server = servers[msg.guild.id];
        if(server.playing){
            msg.reply("Music is already playing");
        }else{
            Play(msg);
        }
        
    }
};