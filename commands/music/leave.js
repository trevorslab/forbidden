const { Command } = require('discord.js-commando');

module.exports = class LeaveVoiceCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'leave',
            group: 'music',
            memberName: 'leave',
            description: 'Leaves the voice channel',
            examples: ['leave']
        });
    }

    run(msg) {
        if(msg.guild.voiceConnection){
            msg.guild.voiceConnection.disconnect();
            msg.reply("I'm Gone")
        }else msg.reply("I am not in a voice channel on this server");
    }
};