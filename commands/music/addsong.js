const { Command } = require('discord.js-commando');

const YTDL = require('ytdl-core')
module.exports = class JoinVoiceCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'add',
            aliases: ['addsong'],
            group: 'music',
            memberName: 'add',
            description: 'Adds a song to the playlist',
            examples: ['add [Video URL]'],
            args: [
                {
                    key: 'link',
                    prompt: 'What song do you want to hear',
                    type: 'string'
                }
            ]
        });
    }

    run(msg, { link }) {
        var server = servers[msg.guild.id];
        YTDL.getInfo(link, function(err, info) {
            server.queue.push(link);
            msg.reply("Added " + info.title + " to the queue");
        });
    }
};