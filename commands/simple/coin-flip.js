const { Command } = require('discord.js-commando');

module.exports = class CoinFlipCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'flip',
            group: 'simple',
            memberName: 'flip',
            description: 'Flips A Coin',
            examples: ['flip']
        });
    }

    run(msg) {
        var coin = Math.floor(Math.random() * 2);
        if(coin == 0){
            msg.reply("Your coin landed on tails");
        }else{
            msg.reply("Your coin landed on heads");
        }
    }
};